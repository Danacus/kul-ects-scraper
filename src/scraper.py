from dataclasses import dataclass
from dataclasses_json import dataclass_json

import yaml

from typing import Any, List, Optional
from bs4 import BeautifulSoup


def noop(self, *args, **kw):
    pass


yaml.emitter.Emitter.process_tag = noop


@dataclass_json
@dataclass
class ProgramBlock:
    title: str
    group: Any


@dataclass_json
@dataclass
class ProgramGroup:
    choice_rules: str
    blocks: List[Any]


@dataclass_json
@dataclass
class Phase:
    phase: Optional[int]
    mandatory: bool


@dataclass_json
@dataclass
class ProgramOpo:
    name: str
    sp: int
    phases: List[Phase]
    semester: str
    code: str
    docents: List[str]


@dataclass_json
@dataclass
class ProgramList:
    opos: List[str]


@dataclass_json
@dataclass
class Data:
    opos: dict
    tree: ProgramGroup


opos = dict()


def parse_semester(sem) -> str:
    img_src = sem.a.img.attrs["src"]
    
    if "-1.png" in img_src:
        return "first"
    if "-2.png" in img_src:
        return "second"
    if "-3.png" in img_src:
        return "both"
    else:
        return "none"


def parse_phase(phase) -> Phase:
    mandatory = "-m.png" in phase.img.attrs["src"]
    classes = phase.attrs["class"]

    if "fase_1" in classes:
        phs = 1
    elif "fase_2" in classes:
        phs = 2
    else:
        phs = None

    return Phase(phs, mandatory)


def parse_opo(row) -> str:
    name = ""
    sp = 0
    semester = "none"
    code = ""
    docents = []

    for cell in row.children:
        if "class" in cell.attrs:
            cls = cell.attrs["class"][0]

            if cls == "sp":
                sp = int(str(cell.text)[:-4])
            elif cls == "opleidingsonderdeel":
                name = str(cell.text)
            elif cls == "sem":
                semester = parse_semester(cell)
            elif cls == "code":
                code = str(cell.text)
            elif cls == "docent":
                docents = [str(li.text) for li in cell.div]

    phases = []

    for phase in row.find_all("a", class_="faselink"):
        phases.append(parse_phase(phase))

    opo = ProgramOpo(name, sp, phases, semester, code, docents)
    opos[code] = opo
    return code


def parse_list(li):
    table = li.table

    opos = []

    for row in table.findChildren("tr", recursive=True):
        if "opo_row" in row.attrs["class"]:
            opos.append(parse_opo(row))

    return ProgramList(opos)


def parse_block(block) -> ProgramBlock:
    div = block.findChildren("div" , recursive=False)[0]
    title = str(div.previous_sibling.previous_sibling.text).strip()
    group = parse_group(div)
    return ProgramBlock(title, group)


def parse_group(group) -> ProgramGroup:
    rules = str(group.findChildren("p", recursive=False)[0].text)

    def parse_li(li) -> Any:
        if li.attrs['class'][0] == "direct-content":
            return parse_list(li)
        else:
            return parse_block(li)

    blocks = [parse_li(li) for li in group.ul.children]
    return ProgramGroup(rules, blocks)


def get_top_div(soup):
    return soup.find("div", class_="programma__content")


@dataclass_json
@dataclass
class ScrapeResult:
    json: str
    yaml: str


def scrape_page(input: str) -> ScrapeResult:
    soup = BeautifulSoup(input, features="lxml")
    top_group = get_top_div(soup)
    tree = parse_group(top_group)

    result = Data(opos, tree)
    yml = yaml.dump(result, allow_unicode=True)
    jsn = result.to_json(indent=2)

    return ScrapeResult(json=jsn, yaml=yml)
