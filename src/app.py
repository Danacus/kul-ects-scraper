from flask import Flask, Response
from flask_cors import CORS

import requests
import scraper
import os
import time

# Max seconds since last modification
MAX_CACHE_AGE = 604800

if 'MAX_CACHE_AGE' in os.environ:
    MAX_CACHE_AGE = int(os.environ['MAX_CACHE_AGE'])

CACHE_DIR = "./cache/"

if 'CACHE_DIR' in os.environ:
    CACHE_DIR = os.environ['CACHE_DIR']

app = Flask(__name__)
CORS(app)


def build_url(year: str, lang: str, code: str) -> str:
    return f"https://onderwijsaanbod.kuleuven.be/{year}/opleidingen/{lang}/{code}.htm"


@app.route("/api/v1/<year>/opleidingen/<lang>/<code>.htm")
@app.route("/api/v1/<year>/opleidingen/<lang>/<code>")
@app.route("/api/v1/<year>/<lang>/<code>")
def get_data(year, lang, code):
    url = build_url(year, lang, code)

    res = requests.get(url)

    data = get_cached(year, lang, code)

    if data is None:
        result = scraper.scrape_page(res.content)
        data = result.json
        save_to_cache(year, lang, code, data)

    return Response(data, 200, mimetype="application/json")


def get_cached(year, lang, code) -> str:
    path = f"{CACHE_DIR}/{year}/{lang}/{code}/ects.json"

    if os.path.isfile(path):
        st = os.stat(path)

        if time.time() - st.st_mtime < MAX_CACHE_AGE:
            with open(path) as f:
                return f.read()

    return None


def save_to_cache(year, lang, code, data):
    dir = f"{CACHE_DIR}/{year}/{lang}/{code}/"

    if not os.path.exists(dir):
        os.makedirs(dir)

    path = f"{dir}/ects.json"

    with open(path, "w") as f:
        return f.write(data)
