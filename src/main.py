import scraper


def main():
    with open("../ects.html", encoding="utf-8") as fp:
        data = fp.read()
        result = scraper.scrape_page(data)

        f = open("../ects.yaml", "w")
        f.write(result.yaml)
        f.close()

        f = open("../ects.json", "w")
        f.write(result.json)
        f.close()


if __name__ == "__main__":
    main()
